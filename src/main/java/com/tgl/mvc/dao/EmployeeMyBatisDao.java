package com.tgl.mvc.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tgl.mvc.model.Employee;

@Repository
public interface EmployeeMyBatisDao {

	Employee inquiryId(long id);

	long insert(Employee employee);

	int  update(Employee employee);

	boolean delete(long id);
	
	@Transactional(rollbackFor = SQLException.class)
	boolean batchInsert(List<Employee> listRec1);
	
}
