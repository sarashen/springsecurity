package com.tgl.mvc.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ValidException extends RuntimeException{
	private static final long serialVersionUID = 1L;
	
	public ValidException(String s) {
		super("欄位值有誤" + s);
	}

}
