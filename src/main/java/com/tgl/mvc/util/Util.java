package com.tgl.mvc.util;

import java.text.DecimalFormat;

public class Util {

	public String maskutil(String input) {
		if (input.isEmpty()) {
			return null;
		}

		StringBuilder sb = new StringBuilder();

		sb.append(input.charAt(0));
		sb.append('X');
		sb.append(input.substring(2));
		return sb.toString();
	} // maskutil end

	public float calcBmi(float high, float weigh) {
		DecimalFormat df = new DecimalFormat("##.00");
		float high1 =  high / 100;
		float bmi = (float) (weigh / (high1 * high1));
		float bmi1 = (float) Double.parseDouble(df.format(bmi));
		return bmi1;
	}
}
