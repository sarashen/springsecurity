package com.tgl.mvc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.AccessDeniedHandler;

import com.tgl.mvc.handler.UserAccessDeniedHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	protected void configure(AuthenticationManagerBuilder auth) throws Exception{
		auth
			.inMemoryAuthentication()
			.withUser("admin").password("{noop}admin").roles("ADMIN")
			.and()
			.withUser("user").password("{noop}user").roles("USER");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception{
		
		http
			.httpBasic()
			.and()
			.authorizeRequests()    // 安全配置
			.antMatchers(HttpMethod.GET, "/employee/**").hasAnyRole("USER","ADMIN")
			.antMatchers(HttpMethod.POST, "/employee/**").hasRole("ADMIN")
			.antMatchers(HttpMethod.PUT, "/employee/**").hasRole("ADMIN")
			.antMatchers(HttpMethod.DELETE, "/employee/**").hasRole("ADMIN")
			.anyRequest().authenticated()
			.and()
			.exceptionHandling().accessDeniedHandler(userAccessDeniedHandler())
			.and()
			.csrf()
			.disable();
		}
	
	@Bean
	public AccessDeniedHandler userAccessDeniedHandler() {
		return new UserAccessDeniedHandler();
	}
}
