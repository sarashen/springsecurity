package com.tgl.mvc.handler;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

//定義403回覆訊息
public class UserAccessDeniedHandler implements AccessDeniedHandler{
	
	@Override
	public void handle(HttpServletRequest request,HttpServletResponse response,
					   AccessDeniedException accessDeniedException) throws IOException,
					   ServletException {
				response.setContentType("application/json);charset=UTF-8");
				response.setCharacterEncoding("UTF-8");
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				PrintWriter out = response.getWriter();
				out.write("{\"status\":\"error\",\"msg\":\"權限不足，請聯絡管理員!\"}");
				out.flush();
				out.close();
				}
	}


