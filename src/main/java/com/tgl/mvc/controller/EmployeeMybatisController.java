package com.tgl.mvc.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.tgl.mvc.exception.RecordNotFoundException;
import com.tgl.mvc.model.Employee;
import com.tgl.mvc.service.EmployeeMybatisService;

import io.swagger.annotations.ApiOperation;

@Controller
@ResponseBody
@RequestMapping(value = "/employee/mybatis")
public class EmployeeMybatisController {

	@Autowired
	private EmployeeMybatisService employeeMyBatisService;
	private static final Logger LOG = LogManager.getLogger(EmployeeMybatisController.class);
	

	@ApiOperation("查詢員工資料接口")
	@GetMapping(value = "/{id}") // Inquiry
	public ResponseEntity<Employee> inquiryId(@PathVariable("id") @Min(1) long employeeId) {
		Employee rs = employeeMyBatisService.inquiryId(employeeId);

		if (rs == null) {
			throw new RecordNotFoundException(employeeId);
		}	
		return new ResponseEntity<>(rs, HttpStatus.OK);

	}

	@ApiOperation("新增員工接口")
	@PostMapping // Insert data
	public ResponseEntity<Long> insert(@RequestBody @Valid Employee employee) {

		long insertId = employeeMyBatisService.insert(employee);
		return new ResponseEntity<>(insertId, HttpStatus.CREATED);
	}

	@ApiOperation("維護員工資料接口")
	@PutMapping
	public ResponseEntity<Employee> update(@RequestBody @Valid Employee employee) {

		Employee rs = employeeMyBatisService.inquiryId(employee.getId());

		if (rs == null) {
			throw new RecordNotFoundException(employee.getId());
		}
		employeeMyBatisService.update(employee);
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}

	@ApiOperation("刪除員工資料接口")
	@DeleteMapping(value = "/{id}") // Delete Data
	public ResponseEntity<Long> delete(@PathVariable("id") @Min(1) long employeeId) {

		Employee rs = employeeMyBatisService.inquiryId(employeeId);
		if (rs == null) {
			throw new RecordNotFoundException(employeeId);
		}
		
		employeeMyBatisService.delete(employeeId);
		return new ResponseEntity<>(employeeId, HttpStatus.OK);
	}
	
	@ApiOperation("批次新增員工資料")
	@PostMapping("/batchInsert")
	public boolean batchInsert(@RequestParam("file") MultipartFile file) {
		if ( file == null || file.isEmpty()) {
			return false;
		}
		List<String> list = new ArrayList<>();
		try (BufferedReader read = new BufferedReader(new InputStreamReader(file.getInputStream(), StandardCharsets.UTF_8))){
				String str;
				while ((str = read.readLine()) != null) {
					list.add(str);
				}
		} catch(IOException e) {
			LOG.error("file:{} , error:{}",file.getName(), e);
		}
		return employeeMyBatisService.batchInsert(list);
	} // end batchInsert 
}
