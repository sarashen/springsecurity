package com.tgl.mvc.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.tgl.mvc.dao.EmployeeDao;
import com.tgl.mvc.exception.ValidException;
import com.tgl.mvc.model.Employee;
import com.tgl.mvc.util.Util;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;

	public Employee inquiryId(long employeeId) {
		Employee rs = employeeDao.inquiryId(employeeId);
		if (rs == null) {
			return null;
		}
		Util util = new Util();
		rs.setCname(util.maskutil(rs.getCname()));
		return rs;
	} // * inquiryId

	public long insert(Employee employee) {
		Util util = new Util();
		float vBmi = util.calcBmi(employee.getHigh(), employee.getWeight());
		employee.setBmi(vBmi);
		return employeeDao.insert(employee);
	} // end insert

	public Employee update(Employee employee) {
		Util util = new Util();
		float vBmi = util.calcBmi(employee.getHigh(), employee.getWeight());
		employee.setBmi(vBmi);
		return employeeDao.update(employee);
	}

	public boolean delete(long employeeId) {
		return employeeDao.delete(employeeId);
	}	
 	
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = SQLException.class)
	public int batchInsert(List<String> listRec) {

			List<Employee> listRec1 = new ArrayList<>();
			for(String list : listRec) {
				String[] strData = list.split(",");
				if( strData == null || strData.length !=6 ) {
					continue;
				}
				System.out.println("Employee Service:"+strData.toString());
				Employee employee = new Employee();
				if (Integer.parseInt(strData[0]) < 130 || Integer.parseInt(strData[0]) > 200 ){
					throw new ValidException("身高: "+ strData[0]);
				}
				if (Integer.parseInt(strData[1]) < 40 || Integer.parseInt(strData[1]) > 120 ){
					throw new ValidException("體重: "+ strData[1]);
				}
				if ( !strData[2].matches("^[a-zA-Z]+-[a-zA-Z]+$")) {
					throw new ValidException("英文姓名: "+ strData[2]);
				}
				if ( !strData[3].matches("^[\\u4e00-\\u9fa5.·]{0,}$")) {
					throw new ValidException("中文姓名: "+ strData[3]);
				}
				if ( !strData[4].matches("^([A-Za-z0-9_\\-\\.])+\\@([A-Za-z0-9_\\-\\.])+\\.([A-Za-z]{2,4})$")) {
					throw new ValidException("e-MAIL: "+ strData[4]);
				}
				if ( !strData[5].matches("^[0-9]{4}")) {
					throw new ValidException("分機: "+ strData[5]);
				}
				employee.setHigh((float)Double.parseDouble(strData[0]));
				employee.setWeight((float)Double.parseDouble(strData[1]));
				employee.setEname(strData[2]);
				employee.setCname(strData[3]);
				employee.setEmail(strData[4]);
				employee.setExt(strData[5]);
				Util util = new Util();
				employee.setBmi(util.calcBmi(employee.getHigh(), employee.getWeight()));
				
				listRec1.add(employee);
				
			}
			return employeeDao.batchInsert(listRec1);
		} // end batchInsert   */
	

}
